package shared.data

/**
  * Represents a point that was drawn on a canvas.
  * <p>
  * Created by Matthias Braun on 5/3/2017.
  */
case class DrawnPoint(x: Double, y: Double)
